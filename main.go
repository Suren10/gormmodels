package main

import (
	"log"

	"bitbucket.org/sovkombank/gormmodels/utils"

	"time"

	conf "bitbucket.org/parkingcrew/parking/conf"
	"bitbucket.org/sovkombank/gormmodels/db/models"
	"bitbucket.org/sovkombank/gormmodels/rest"
	"github.com/Sirupsen/logrus"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var config string

func init() {
	conf.Conf.AppMod = "dev"
}

func main() {

	models.InitModels()
	var err error
	time.Local, err = time.LoadLocation("Europe/Moscow")
	if err != nil {
		log.Fatal(err)
	}
	if err != nil {
		log.Fatal(err)
	}
	// client := &models.Client{
	// 	Phone:            "+79872352077",
	// 	SMSCode:          "2632",
	// 	PublicEncryptKey: uuid.Must(uuid.NewV4()),
	// 	JoinDate:         time.Now(),
	// }
	// err := client.Create()
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// account := &models.Account{
	// 	Name:          "Suren",
	// 	Status:        0,
	// 	IconURL:       "-",
	// 	RefuseComment: "-",
	// 	RepDate:       time.Now(),
	// }
	// err = account.Create()
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// client := &models.Client{
	// 	Phone: "+79872352077",
	// }
	// err := client.FindByPhone()
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(client.Account)
	// client.AccountID = account.ID
	// err = client.Update()
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// devices, err := models.FindAllDevicesByAccountID(384)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// for _, device := range devices {
	// 	device.OS = "test OS"
	// 	err = device.Update()
	// 	if err != nil {
	// 		fmt.Println(err)
	// 	}
	// }
	// score, err := models.GetUserRating(384)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(score)

	// averload, err:= models.GetPaymentByAccountIdAndClosed(384)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(averload)

	// credreq := &models.CreditRequest{
	// AccountID: 400,
	// RequestState:1,
	// RequestType: 600,
	// }

	// lid, err := credreq.Create()

	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(lid)

	//	defer db.DbSession.Close()
	sender := utils.NewSmsSender("http://localhost:4000/sms", logrus.New())
	rest.StartRestServices(sender)
}
