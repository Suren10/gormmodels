package rest

import (
	"math/rand"

	"bitbucket.org/sovkombank/gormmodels/utils"
	"github.com/Sirupsen/logrus"

	"time"

	"github.com/gin-gonic/gin"
)

func init() {
	rand.Seed(time.Now().Unix())
}

type handlerManager struct {
	smsService *utils.SMSSender
	logger     *logrus.Logger
}

func StartRestServices(sender *utils.SMSSender) {
	//AVG rep 08.05.18
	router := gin.New()
	router.Use(gin.Logger(), gin.Recovery())
	// router.Use(Logger(handler.dbmanager))
	v1 := router.Group("/api/v1")
	handler := handlerManager{
		smsService: sender,
		logger:     logrus.New(),
	}
	// v1.Use(MoneyRoutes())
	// v1.Use(MyRecovery())

	// v1.POST("/login", handler.authenticate)
	// v1.POST("/touchLogin", handler.touchIDAuth)
	// v1.POST("/smsResend", handler.resendSMS)
	// v1.GET("/answers", handler.getAnswersMap)
	// v1.GET("/answer/:id", handler.getAnswer)
	// v1.GET("/creditType", handler.getCreditIcon)
	// v1.POST("/notification", handler.addNewNotification)
	// v1.POST("/restorePassword", handler.resetPassword)
	// v1.GET("/departments", handler.getDepartmentsHandler)

	userRouter := v1.Group("/user")
	{
		registerRouter := userRouter.Group("/register")
		{
			registerRouter.POST("/new", handler.RegistrationsHandler) //19ms avg
		}
	}
	router.Run(":3000")
}
