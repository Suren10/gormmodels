package rest

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"bitbucket.org/sovkombank/gormmodels/db/models"
	"bitbucket.org/sovkombank/gormmodels/utils"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"go.uber.org/zap"
)

func (h *handlerManager) RegistrationsHandler(c *gin.Context) {
	var phone struct {
		Phone string `json:"phone"`
	}
	err := c.BindJSON(&phone)
	if err != nil {
		h.logger.Warn("Warning! Cant decode json", zap.Error(err))
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	client := &models.Client{
		Phone: phone.Phone,
	}

	err = client.FindByPhone()
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			SMSCode := fmt.Sprintf("%d%d%d%d", rand.Int31n(9), rand.Int31n(9), rand.Int31n(9), rand.Int31n(9))
			SMSMessage := fmt.Sprintf(`Код для подтверждения: %s`, SMSCode)
			h.smsService.SmsRequestInputChan <- utils.SmsRequest{Phone: phone.Phone, Text: SMSMessage}

			client := &models.Client{
				Phone:            phone.Phone,
				SMSCode:          SMSCode,
				PublicEncryptKey: uuid.Must(uuid.NewV4()),
				JoinDate:         time.Now(),
				PushSettings:     31,
			}
			err = client.Create()
			if err != nil {
				h.logger.Warn("Warning! Cant create new client", zap.Error(err))
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}
			return
		} else {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
	}
	if !client.Deleted && client.IsActive {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "Данный номер уже зарегистрирован в системе"})
		return
	}
	if client.Deleted {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "Данный номер уже зарегистрирован в системе"})
		return
	}
	fmt.Println(client.JoinDate.Sub(time.Now()) > (1 * time.Minute))
	if client.JoinDate != time.Unix(0, 0) && client.JoinDate.Sub(time.Now()) > (1*time.Minute) {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error": "С момента послесдней попытки зарегистрироваться прошло менее 1 минуты!"})
		return
	}
	SMSCode := fmt.Sprintf("%d%d%d%d", rand.Int31n(9), rand.Int31n(9), rand.Int31n(9), rand.Int31n(9))
	SMSMessage := fmt.Sprintf(`Код для подтверждения: %s`, SMSCode)
	h.smsService.SmsRequestInputChan <- utils.SmsRequest{Phone: phone.Phone, Text: SMSMessage}
	// client.JoinDate = time.Now()
	client.SMSCode = SMSCode
	err = client.Update()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
}
