package conf

import (
	"fmt"
)

//Conf - general configuration
var Conf = conf{
	HTTPServerHost: "localhost",
	HTTPServerPort: "9000",
	AppName:        "sovkom_chat",
	AppMod:         "dev",
}

type conf struct {
	AppName            string
	AppMod             string //AppMod - dev/prod/test
	AppVersion         string
	HTTPServerHost     string
	HTTPServerPort     string
	pgUser     string
	pgDbPassword string
	pgHost     string
	pgPort     string
	DbName     string
	EmailHost          string
	EmailPort          int
	EmailLogin         string
	EmailPassword      string
}

func init() {
	fmt.Println(Conf.AppMod)
	switch Conf.AppMod {
	case "prod":
		confDbIniter("root", "qwertasdf",
			"uhq-crcal-db1.sovcombank.group",
			"26257", "parking")
	case "dev":
		confDbIniter("postgres", "",
			"localhost",
			"5432", "suren")
	case "test":
		confDbIniter("postgres", "qwertasdf",
			"rdc-crcal-test-db1",
			"5432", "credithistory")
	}
}

func confDbIniter(pguser, pgpassword, pghost, pgport, pgdbname string) {
	Conf.pgUser = pguser
	Conf.pgDbPassword = pgpassword
	Conf.pgHost = pghost
	Conf.pgPort = pgport
	Conf.DbName = pgdbname
}

func (configure conf) HTTPConnectionString() string {
	return fmt.Sprintf("%v:%v", configure.HTTPServerHost, configure.HTTPServerPort)
}

func (configure conf) PgDbConnectionString() string {
	return fmt.Sprintf("postgresql://%v@%v:%v/%v?sslmode=disable",
		configure.pgUser, configure.pgHost,
		configure.pgPort, configure.DbName,
	)
}
