package utils

import (
	"bytes"
	"fmt"
	"net/http"

	"encoding/json"

	"io/ioutil"

	"github.com/Sirupsen/logrus"
)

type SmsRequest struct {
	Phone string
	Text  string
}

type SMSSender struct {
	serverAddress       string
	SmsRequestInputChan chan SmsRequest
	logger              *logrus.Logger
}

func NewSmsSender(addr string, logger *logrus.Logger) *SMSSender {
	return &SMSSender{
		serverAddress:       addr,
		SmsRequestInputChan: make(chan SmsRequest, 10),
		logger:              logger,
	}
}

func (s *SMSSender) StartSender() {
	for smsRequest := range s.SmsRequestInputChan {
		err := s.sendSMS(smsRequest.Phone, smsRequest.Text)
		if err != nil {
			s.logger.WithFields(logrus.Fields{"Error": err.Error()}).Warn("Warning! Cant' send sms!")

		}
	}
}

func (s *SMSSender) sendSMS(phone, text string) error {
	var smsMap = map[string]string{
		"phone": phone,
		"text":  text,
	}
	data, err := json.Marshal(&smsMap)
	if err != nil {
		return err
	}
	response, err := http.Post(s.serverAddress, "application/json", bytes.NewBuffer([]byte(data)))
	if err != nil {
		return err
	}
	defer response.Body.Close()
	if response.StatusCode != 200 {
		data, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return err
		}
		return fmt.Errorf("Warning! Cant send message,server returned code, %d. Error message %s", response.StatusCode, string(data))
	}
	return nil
}

func (s *SMSSender) SendSMS(sms SmsRequest) error {
	return s.sendSMS(sms.Phone, sms.Text)
}
