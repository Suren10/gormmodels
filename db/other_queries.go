package db

type CreditRating struct {
	Score       int    `json:"score"`
	Commentary  string `json:"commentary"`
	Description string `json:"description"`
}

func GetCreditScoreParameters(accountID int64) (*CreditRating, error) {
	score := &CreditRating{}
	var Result struct {
		CreditRating int
		Commentary   string
		Description  string
	}

	err := DbSession.Raw("SELECT credit_rating,commentary,description FROM credit_score WHERE account_id = ?", accountID).Scan(&Result).Error
	if err != nil {
		return nil, err
	}
	score.Score = Result.CreditRating
	score.Commentary = Result.Commentary
	score.Description = Result.Description

	return score, nil
}
