package models

import "time"

type SmsNotify struct {
	TableName    struct{} `sql:"sms_notify"`
	ID           int64    `pk:"autoincr"`
	Account      *Account `pg:"fk:account_id" json:"-"`
	Text         string
	FutureSendDT time.Time
	RealSendDT   time.Time
	Handled      bool
	AttemptCount int64
	ErrorText    string
}
