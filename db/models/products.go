package models

type Products struct {
	TableName    struct{} `table:"products"`
	ID           int64    `pk:"autoincr"`
	Name         string
	MaxCondition int
}
