package models

import (
	"time"

	"bitbucket.org/sovkombank/gormmodels/db"
)

type Account struct {
	ID            int64 `gorm:"primary_key;AUTO_INCREMENT"`
	Name          string
	Status        int
	IconURL       string
	RefuseComment string
	PyrusTaskID   int
	RepDate       time.Time
}

func (Account) TableName() string {
	return "account"
}

func (account *Account) Create() error {
	return db.DbSession.Create(&account).Error
}

func (account *Account) Update() error {
	return db.DbSession.Save(&account).Error
}

func (account *Account) FindById() error {
	return db.DbSession.Where("id=?", account.ID).First(&account).Error
}