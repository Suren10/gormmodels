package models

import (
	"time"

	"github.com/satori/go.uuid"
)

type ProposalNotification struct {
	TableName struct{} `sql:"proposal_notification"`
	ID        int64    `pk:"autoincr"`
	Title     string
	Text      string
	Category  string
	Show      bool
	Redirect  bool
	UUID      uuid.UUID
	State     int
	Account   *Account `pg:"fk:account_id`
	ParentID  int
	Date      time.Time
}
