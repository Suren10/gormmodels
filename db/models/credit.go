package models

import (
	"database/sql"
	"time"

	"bitbucket.org/sovkombank/gormmodels/db"
)

type Credit struct {
	ID                   int64     `gorm:"primary_key;AUTO_INCREMENT"`
	Name                 string    `json:"name"`
	UUID                 string    `json:"-"`
	Type                 string    `json:"type"`
	TypeID               int       `json:"typeID"`
	DateOpen             time.Time `json:"dateOpen"`
	DateClose            time.Time `json:"dateClose"`
	Closed               bool      `json:"closed"`
	DateActualClosed     time.Time `json:"dateActualClosed"`
	BalanceOwned         int       `json:"balanceOwned"`
	LoadAmount           int       `json:"loadAmount"`
	Term                 int       `json:"term"`
	Payment              int       `json:"payment"`
	DatePay              int       `json:"datePay"`
	DelayAmount          int       `json:"delayAmount"`
	DateNextPayment      time.Time `json:"dateNextPayment"`
	DateLastUpdate       time.Time `json:"dateLastUpdate"`
	DateLoad             time.Time `json:"-"`
	PercentageRage       int       `json:"percentageRage"`
	PercentageGroup      string    `json:"percentageGroup"`
	Show                 bool      `json:"show"`
	RepDate              time.Time `json:"-"`
	Account              *Account  `gorm:"foreignkey:AccountID"`
	AccountID            int64
	CreditVersion        int         `json:"-"`
	Last                 bool        `json:"-"`
	CreditGUID           string      `json:"uuid"`
	BuroCredit           *BuroCredit `gorm:"foreignkey:BuroCreditID"`
	BuroCreditID         int64
	CreditState          int    `json:"status"`
	ReceiveNotifications bool   `json:"receive_notifications"`
	BuroCreditUUID       string `gorm:"-" json:"buroCreditUuid"`
}

func (Credit) TableName() string {
	return "credit"
}

func (credit *Credit) Create() error {
	return db.DbSession.Create(&credit).Error
}

func (credit *Credit) GetCreditByUUIDAndAccountID() error {
	err := db.DbSession.Where("account_id = ? AND credit_guid = ?", credit.AccountID, credit.CreditGUID).First(&credit).Error
	if err != nil {
		return err
	}
	credit.BuroCredit = &BuroCredit{
		ID: credit.BuroCreditID,
	}
	return credit.BuroCredit.FindById()
}

func (credit *Credit) Update() error {
	return db.DbSession.Save(&credit).Error
}

func getCreditsByAccountIDAndDate(accountID, date int64) ([]Credit, error) {
	query := "SELECT * FROM credit WHERE account_id = ? AND last = TRUE AND show = TRUE"
	rows := new(sql.Rows)
	var err error
	if date > 0 {
		query += " AND date_load > ?"
		rows, err = db.DbSession.Raw(query, accountID, time.Unix(date, 0)).Rows()
	} else {
		rows, err = db.DbSession.Raw(query, accountID).Rows()
	}
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var buroCreditID sql.NullInt64
	resulst := make([]Credit, 0)
	for rows.Next() {
		credit := Credit{}
		err := rows.Scan(&credit.ID, &credit.Name, &credit.UUID, &credit.Type, &credit.TypeID, &credit.DateOpen, &credit.DateClose, &credit.Closed, &credit.DateActualClosed,
			&credit.BalanceOwned, &credit.LoadAmount, &credit.Term, &credit.Payment, &credit.DatePay, &credit.DelayAmount, &credit.DateNextPayment, &credit.DateLastUpdate,
			&credit.DateLoad, &credit.PercentageRage, &credit.PercentageGroup, &credit.Show, &credit.RepDate, &credit.AccountID, &credit.CreditVersion,
			&credit.Last, &credit.CreditGUID, &buroCreditID, &credit.CreditState, &credit.ReceiveNotifications)
		if err != nil {
			return nil, err
		}
		if buroCreditID.Valid {
			credit.BuroCreditID = buroCreditID.Int64
		}
		resulst = append(resulst, credit)
	}
	return resulst, nil
}
