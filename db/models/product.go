package models

import (
	"github.com/satori/go.uuid"
)

type Product struct {
	TableName struct{} `sql:"product"`
	ID        int64    `pk:"autoincr"`
	Name      string
	Text      string
	Price     int
	IsLast    bool
	ColorHex  string
	IconName  string
	UUID      uuid.UUID
	Show      bool
}
