package models

import (
	"time"

	"bitbucket.org/sovkombank/gormmodels/db"
)

type RequestDirection struct {
	ID                  int64 `gorm:"primary_key;AUTO_INCREMENT"`
	CreditRequestID     int64
	Version             int64
	FlagLast            bool
	CreditAmt           int64
	CreditTerm          int64
	CreditDownpay       int64
	CreditApr           float32
	CreditPayment       int64
	ClientTimeZone      int64
	FlgZalog            bool
	FlgHasDocuments     bool
	ZalogObjAmt         int64
	Surname             string
	Name                string
	Patronymic          string
	Birthday            time.Time
	Email               string
	PlaceBirth          string
	PassportSeries      string
	PassportNumber      string
	IssuedPlace         string
	IssuedDate          time.Time
	CodeSubdivision     string
	RegistrationAddress string
	House               string
	Building            string
	Flat                string
	RegistrationCity    string
	RegistrationIndex   string
	CallMethod          int64
}

func (RequestDirection) TableName() string {
	return "request_direction"
}

func (requestDirection *RequestDirection) Create() error {
	return db.DbSession.Create(&requestDirection).Error
}

func (requestDirection *RequestDirection) Update() error {
	return db.DbSession.Save(&requestDirection).Error
}