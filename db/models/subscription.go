package models

import "bitbucket.org/sovkombank/gormmodels/db"

type Subscription struct {
	ID        int64    `gorm:"primary_key;AUTO_INCREMENT"`
	Account   *Account `gorm:"foreignkey:AccountID"`
	AccountID int64
	Product   *Product `gorm:"foreignkey:ProductID"`
	ProductID int64
	Last      bool
}

func (Subscription) TableName() string {
	return "subscription"
}

func (subscription *Subscription) Create() error {
	return db.DbSession.Create(&subscription).Error
}

func (subscription *Subscription) FindByAccountID() error {
	return db.DbSession.Where("account_id=?", subscription.AccountID).First(&subscription).Error
}

func (subscription *Subscription) Update() error {
	return db.DbSession.Save(&subscription).Error
}
