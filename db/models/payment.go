package models

import (
	"time"

	"bitbucket.org/sovkombank/gormmodels/db"
)

type Payment struct {
	ID           int64 `gorm:"primary_key;AUTO_INCREMENT"`
	Date         time.Time
	Payment      int
	BalanceOwned int
	LoadAmount   int
	BuroCredit   *BuroCredit `gorm:"foreignkey:BuroCreditID"`
	BuroCreditID int64
	RepDate      time.Time
}

func (Payment) TableName() string {
	return "payment"
}

func FindPaymentsByBuroCreditID(buroCreditID int64) ([]*Payment, error) {
	payments := make([]*Payment, 0)
	err := db.DbSession.Where("buro_credit_id=?", buroCreditID).Find(&payments).Error
	if err != nil {
		return nil, err
	}
	return payments, nil
}
