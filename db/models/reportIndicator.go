package models

import (
	"time"
)

type ReportIndicator struct {
	TableName       struct{} `sql:"report_indicator"`
	ID              int64    `pk:"autoincr"`
	Type            int
	Name            string
	IndicatorID     int
	IndicatorName   string
	MinValue        int
	MaxValue        int
	ShortComment    string
	Comment         string
	Recommendations string
	RepDate         time.Time
	Account         *Account `pg:"fk:account_id"`
}
