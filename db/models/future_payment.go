package models

import (
	"time"

	"bitbucket.org/sovkombank/gormmodels/db"
)

type FuturePayment struct {
	ID          int64 `gorm:"primary_key;AUTO_INCREMENT"`
	Date        time.Time
	PaymentUUID string
	Status      int
	Credit      *Credit `gorm:"foreignkey:CreditID"`
	CreditID    int64
	RepDate     time.Time
}

func (FuturePayment) TableName() string {
	return "future_payment"
}

func FindFuturePaymentsByCreditID(creditID int64) ([]*FuturePayment, error) {
	futurePayments := make([]*FuturePayment, 0)
	err := db.DbSession.Where("credit_id=?", creditID).Find(&futurePayments).Error
	if err != nil {
		return nil, err
	}
	return futurePayments, nil
}

func updateFuturePayment(creditID int64, futurePaymentUUID string, status int) error {
	err := db.DbSession.Exec("UPDATE future_payment SET status = ? WHERE credit_id = ? AND payment_uuid = ?", status, creditID, futurePaymentUUID).Error
	return err
}
