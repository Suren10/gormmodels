package models

import (
	"fmt"
	"time"

	"bitbucket.org/sovkombank/gormmodels/db"

	"github.com/satori/go.uuid"
)

type Client struct {
	ID               int64 `gorm:"primary_key;AUTO_INCREMENT"`
	Phone            string
	IsStaff          bool
	JoinDate         time.Time
	BlockedUntil     time.Time
	PhoneConfirmed   bool
	IsActive         bool
	Password         string
	Account          *Account `gorm:"foreignkey:AccountID"`
	AccountID        int64
	SMSCode          string
	PublicEncryptKey uuid.UUID
	ForgetPassword   bool
	Deleted          bool
	PushSettings     int64
}

func (Client) TableName() string {
	return "client"
}

func (client *Client) Create() error {
	fmt.Println("client created")
	return db.DbSession.Create(&client).Error
}

func (client *Client) Update() error {
	return db.DbSession.Save(&client).Error
}

func (client *Client) FindByPhone() error {
	err := db.DbSession.Where("phone=?", client.Phone).First(&client).Error
	if err != nil {
		return err
	}
	client.Account = &Account{
		ID: client.AccountID,
	}
	return client.Account.FindById()
}

func GetUserPushSettings(accountID int64) (int64, error) {
	var Result struct {
		PushSettings int64
	}
	err := db.DbSession.Raw("SELECT push_settings FROM client WHERE account_id = ?", accountID).Scan(&Result).Error
	if err != nil {
		return 0, err
	}
	return Result.PushSettings, nil
}
