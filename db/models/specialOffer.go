package models

type SpecialOffer struct {
	TableName      struct{} `sql:"special_offer"`
	ID             int64    `pk:"autoincr"`
	UUID           string
	PercentageRate int
	MinSum         int
	MaxSum         int
	MinDuration    int
	MaxDuration    int
	MinDeposit     int
	MaxDeposit     int
	Name           string
	Description    string
	Conditions     []*Conditions
}
