package models

import (
	"time"

	"bitbucket.org/sovkombank/gormmodels/db"
)

type Report struct {
	ID                int64 `gorm:"primary_key;AUTO_INCREMENT"`
	TypeID            int
	Name              string
	Val               string
	SortID            int
	Date              time.Time
	Text              string
	Last              bool
	ReportIndicatorID int
	RepDate           time.Time
	Account           *Account `gorm:"foreignkey:AccountID"`
	AccountID         int64
}

func (Report) TableName() string {
	return "report"
}

func getUserRating(accountID int64) (int, error) {
	var Result struct {
		Val int
	}
	err := db.DbSession.Order("rep_date desc").Limit(1).Raw("SELECT val FROM report WHERE account_id = ? AND type_id = 1", accountID).Scan(&Result).Error
	if err != nil {
		return Result.Val, err
	}
	return Result.Val, nil
}
