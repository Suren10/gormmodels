package models

type CreditState struct {
	TableName   struct{} `sql:"credit_state"`
	ID          int64    `pk:"autoincr" json:"-"`
	StateNumber int64
	StateText   string
}
