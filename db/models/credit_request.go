package models

import (
	"time"

	"bitbucket.org/sovkombank/gormmodels/db"
)

type CreditRequest struct {
	ID                  int64    `gorm:"primary_key;AUTO_INCREMENT"`
	Account             *Account `gorm:"foreignkey:AccountID"`
	AccountID           int64
	RequestDate         time.Time
	RequestType         int64
	PyrusID             int64
	ResponseID          int64
	CRMStatus           int64
	CRMContacted        int64
	CRMAgree            int64
	CRMSigned           int64
	RequestState        int64
	FlgDirectionChanged bool
	StateChanged        time.Time
	StateText           string
	History             []*CreditState `json:"history,omitempty"`
}

func (CreditRequest) TableName() string {
	return "credit_request"
}

func (creditRequest *CreditRequest) Create() (int64, error) {
	err := db.DbSession.Create(&creditRequest).Error
	if err != nil {
		return 0, err
	}
	return creditRequest.ID, nil
}

func (creditRequest *CreditRequest) Update() error {
	return db.DbSession.Save(&creditRequest).Error
}
