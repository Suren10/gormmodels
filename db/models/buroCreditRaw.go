package models

import "time"

type BuroCreditRAW struct {
	ID               int64     `pk:"autoincr" json:"-"`
	Name             string    `json:"name"`
	UUID             string    `json:"uuid"`
	Type             string    `json:"type"`
	TypeID           int       `json:"typeID"`
	DateOpen         time.Time `json:"dateOpen"`
	DateClose        time.Time `json:"dateClose"`
	Closed           bool      `json:"closed"`
	DateActualClosed time.Time `json:"dateActualClosed"`
	BalanceOwned     int       `json:"balanceOwned"`
	LoadAmount       int       `json:"loadAmount"`
	Term             int       `json:"term"`
	Payment          int       `json:"payment"`
	DatePay          int       `json:"datePay"`
	DelayAmount      int       `json:"delayAmount"`
	DateNextPayment  time.Time `json:"dateNextPayment"`
	DateLastUpdate   time.Time `json:"dateLastUpdate"`
	DateLoad         time.Time `json:"-"`
	PercentageRage   int       `json:"percentageRage"`
	PercentageGroup  string    `json:"percentageGroup"`
	Show             bool      `json:"show"`
	RepDate          time.Time `json:"-"`
	Account          int64     `json:"-"`
	CreditVersion    int       `json:"-"`
	Last             bool      `json:"-"`
	CreditGUID       string
	CreditState      int `json:"status"`
}
