package models

import (
	"time"

	"bitbucket.org/sovkombank/gormmodels/db"
)

type Notification struct {
	ID       int64     `gorm:"primary_key;AUTO_INCREMENT"`
	Title    string    `json:"title"`
	Text     string    `json:"text"`
	Category string    `json:"category"`
	Show     bool      `json:"show"`
	Redirect bool      `json:"redirect"`
	UUID     string    `json:"uuid"`
	State    int       `json:"state"`
	Account  *Account  `pg:"fk:account_id" json:"-"`
	ParentID int       `json:"parentID"`
	Date     time.Time `json:"date"`
}

func (Notification) TableName() string {
	return "notification"
}

func FindAllNotificationsByAccountID(accountID int64) (error, []*Notification) {
	notifications := make([]*Notification, 0)
	err := db.DbSession.Where("account_id=? AND show=?", accountID, true).Find(&notifications).Error
	if err != nil {
		return err, nil
	}
	return nil, notifications
}
