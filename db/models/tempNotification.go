package models

import (
	"time"
)

type TempNotification struct {
	TableName     struct{}  `sql:"temp_notify" json:"-"`
	ID            int64     `pk:"autoincr" json:"-"`
	Category      string    `json:"category"`
	Header        string    `json:"header"`
	Text          string    `json:"text"`
	Redirect      string    `json:"redirect"`
	Handled       bool      `json:"-"`
	UUID          string    `sql:"notification_guid" json:"uuid"`
	Account       *Account  `fk:"account_id,inverse" json:"-"`
	CategoryID    int64     `json:"category_id,omitempty"`
	FutureSendDT  time.Time `json:"future_send_dt,omitempty"`
	RealSendDT    time.Time `json:"real_send_dt,omitempty"`
	AttemptToSend int64
	Error         string
}
