package models

type Department struct {
	TableName    struct{} `sql:"department"`
	ID           int64    `pk:"autoincr"`
	Name         string
	Latitude     float32
	Longitude    float32
	WorkingHours string
}
