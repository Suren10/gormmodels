package models

import (
	"time"

	"bitbucket.org/sovkombank/gormmodels/db"
)

type DeleteReason struct {
	ID        int64 `gorm:"primary_key;AUTO_INCREMENT"`
	AccountID int64
	Reason    string
	Date      time.Time
}

func (DeleteReason) TableName() string {
	return "delete_reason"
}

func (deleteReason *DeleteReason) Create() error {
	return db.DbSession.Create(&deleteReason).Error
}
