package models

import (
	"time"

	"github.com/satori/go.uuid"
)

type CreditNotification struct {
	TableName        struct{}  `sql:"credit_notification"`
	ID             int64          `pk:"autoincr"`
	Credit         *Credit        `fk:"credit_id,inverse"`
	Account        *Account       `pg:"fk:account_id"`
	FuturePayment  *FuturePayment `fk:"future_payment_id,inverse"`
	UUID           uuid.UUID
	ExecuteDate    time.Time
	TimeOffset     float32
	CreditSettings *CreditSettings `fk:"credit_settings_id,inverse"`
	Actual         bool
	Send           bool
}