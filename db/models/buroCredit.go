package models

import (
	"database/sql"
	"time"

	"bitbucket.org/sovkombank/gormmodels/db"
)

type BuroCredit struct {
	ID               int64     `gorm:"primary_key;AUTO_INCREMENT"`
	Name             string    `json:"name"`
	UUID             string    `json:"uuid"`
	Type             string    `json:"type"`
	TypeID           int       `json:"typeID"`
	DateOpen         time.Time `json:"dateOpen"`
	DateClose        time.Time `json:"dateClose"`
	Closed           bool      `json:"closed"`
	DateActualClosed time.Time `json:"dateActualClosed"`
	BalanceOwned     int       `json:"balanceOwned"`
	LoadAmount       int       `json:"loadAmount"`
	Term             int       `json:"term"`
	Payment          int       `json:"payment"`
	DatePay          int       `json:"datePay"`
	DelayAmount      int       `json:"delayAmount"`
	DateNextPayment  time.Time `json:"dateNextPayment"`
	DateLastUpdate   time.Time `json:"dateLastUpdate"`
	DateLoad         time.Time `json:"-"`
	PercentageRage   int       `json:"percentageRage"`
	PercentageGroup  string    `json:"percentageGroup"`
	Show             bool      `json:"show"`
	RepDate          time.Time `json:"-"`
	Account          *Account  `gorm:"foreignkey:AccountID"`
	AccountID        int64
	CreditVersion    int  `json:"-"`
	Last             bool `json:"-"`
	CreditGUID       string
	CreditState      int `json:"status"`
}

func (BuroCredit) TableName() string {
	return "buro_credit"
}

func (burocredit *BuroCredit) FindById() error {
	return db.DbSession.Where("id=?", burocredit.ID).First(&burocredit).Error
}

func (burocredit *BuroCredit) Update() error {
	return db.DbSession.Save(&burocredit).Error
}

func (burocredit *BuroCredit) FindByUUID() error {
	return db.DbSession.Where("uuid=?", burocredit.UUID).First(&burocredit).Error
}

func getBuroCreditsByAccountIDAndDate(accountID, date int64) ([]*BuroCredit, error) {
	query := "SELECT * FROM buro_credit WHERE account_id = ? AND last = TRUE AND show = TRUE"
	rows := new(sql.Rows)
	var err error
	if date > 0 {
		query += " AND date_load > $?"
		rows, err = db.DbSession.Raw(query, accountID, time.Unix(date, 0)).Rows()
	} else {
		rows, err = db.DbSession.Raw(query, accountID).Rows()
	}
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	result := make([]*BuroCredit, 0)
	for rows.Next() {
		cred := &BuroCredit{}
		err = rows.Scan(&cred.ID, &cred.Name, &cred.UUID, &cred.Type, &cred.TypeID, &cred.DateOpen, &cred.DateClose, &cred.Closed, &cred.DateActualClosed, &cred.BalanceOwned,
			&cred.LoadAmount, &cred.Term, &cred.Payment, &cred.DatePay, &cred.DelayAmount, &cred.DateNextPayment, &cred.DateLastUpdate, &cred.DateLoad,
			&cred.PercentageRage, &cred.PercentageGroup, &cred.Show, &cred.RepDate, &cred.Account, &cred.CreditVersion, &cred.Last, &cred.CreditGUID, &cred.CreditState)
		if err != nil {
			return nil, err
		}
		result = append(result, cred)
	}
	return result, nil
}

func GetPaymentByAccountIdAndClosed(accountID int64) (int, error) {
	type Result struct {
		Payment int
	}
	averageLoad := 0
	var results []Result
	err := db.DbSession.Raw("SELECT payment FROM buro_credit WHERE account_id = ? AND closed = FALSE", accountID).Scan(&results).Error
	if err != nil {
		return 0, err
	}
	for _, res := range results {
		averageLoad += res.Payment
	}
	return averageLoad, nil
}
