package models

type CreditSettings struct {
	TableName         struct{} `sql:"credit_settings"`
	ID                int64    `pk:"autoincr"`
	Credit            *Credit  `fk:"credit_id,inverse"`
	Account           *Account `pg:"fk:account_id"`
	TimeOffset        int
	InDay             bool
	OffsetForOneDay   bool
	OffsetForThreeDay bool
	OffsetForFiveDay  bool
}
