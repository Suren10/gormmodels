package models

type RequestDocuments struct {
	TableName           struct{} `sql:"request_documents"`
	ID                  int64    `pk:"autoincr"`
	DirectionID         int64
	DocumentType        int64
	DocumentState       int64
	DocumentDescription string
	DocumentBody        string
}
