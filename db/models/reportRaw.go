package models

import "time"

type ReportRAW struct {
	ID                int64 `pk:"autoincr"`
	TypeID            int
	Name              string
	Val               string
	SortID            int
	Date              time.Time
	Text              string
	Last              bool
	ReportIndicatorID int
	RepDate           time.Time
	Account           int64
}
