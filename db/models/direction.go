package models

import (
	"fmt"
	"time"

	"bitbucket.org/sovkombank/gormmodels/db"
)

type Direction struct {
	ID                  int64 `gorm:"primary_key;AUTO_INCREMENT"`
	RequestID           string
	Surname             string
	Name                string
	Patronymic          string
	Birthday            time.Time
	Email               string
	BalanceAmt          int64
	PlaceBirth          string
	PassportSeries      string
	PassportNumber      string
	IssuedPlace         string
	IssuedDate          time.Time
	CodeSubdivision     string
	RegistrationAddress string
	House               string
	Building            string
	Flat                string
	PhotoClient         string
	PhotoPassport       string
	Account             *Account `gorm:"foreignkey:AccountID"`
	AccountID           int64
	Income              int
	Gender              string
	SNILS               string
	RegistrationCity    string
	RegistrationIndex   string
	VerifyResult        string
	VerifyConfidence    float32
}

func (Direction) TableName() string {
	return "direction"
}

func (direction *Direction) Create() error {
	fmt.Println("direction created")
	return db.DbSession.Create(&direction).Error

}

func (direction *Direction) Update() error {
	return db.DbSession.Save(&direction).Error
}

func (direction *Direction) GetDirectionByAccountID() error {
	return db.DbSession.Where("id=?", direction.AccountID).First(&direction).Error
}

func GetIncome(accountID int64) (int, error) {
	var Result struct {
		Income int
	}
	err := db.DbSession.Raw("SELECT income FROM direction WHERE account_id = ?", accountID).Scan(&Result).Error
	if err != nil {
		return Result.Income, err
	}
	return Result.Income, nil
}
