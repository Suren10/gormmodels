package models

import "bitbucket.org/sovkombank/gormmodels/db"

type Device struct {
	ID        int64 `gorm:"primary_key;AUTO_INCREMENT"`
	Token     string
	Type      string
	OS        string
	Account   *Account `gorm:"foreignkey:AccountID"`
	AccountID int64
}

func (Device) TableName() string {
	return "device"
}

func (device *Device) Create() error {
	return db.DbSession.Create(&device).Error
}

func (device *Device) Update() error {
	return db.DbSession.Save(&device).Error
}

func FindAllDevicesByAccountID(accountID int64) ([]*Device, error ) {
	devices := make([]*Device, 0)
	err := db.DbSession.Where("account_id=?", accountID).Find(&devices).Error
	if err != nil {
		return nil, err
	}
	return devices, nil
}
