package models

import (
	"bitbucket.org/sovkombank/gormmodels/db"
)

func InitModels() {
	db.DbSession.AutoMigrate(&Account{})
	db.DbSession.AutoMigrate(&Client{})
	db.DbSession.AutoMigrate(&Device{})
	db.DbSession.AutoMigrate(&Report{})
	db.DbSession.AutoMigrate(&BuroCredit{})
	db.DbSession.AutoMigrate(&CreditRequest{})
}
