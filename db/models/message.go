package models

import (
	"time"

	"github.com/satori/go.uuid"
)

type Message struct {
	TableName     struct{} `sql:"message"`
	ID            int64    `pk:"autoincr"`
	MessageRole   string
	MessageText   string
	DateSend      time.Time
	DateReceived  time.Time
	MessageStatus int
	MessageUUID   uuid.UUID
	Account       *Account `pg:"fk:account_id"`
}
