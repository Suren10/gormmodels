package models

type Conditions struct {
	TableName      struct{} `sql:"conditions"`
	ID             int64    `pk:"autoincr"`
	Description    string
	ForUser        bool
	SpecialOfferID int64
}
