package models

type ReportType struct {
	TableName struct{} `sql:"report_type"`
	ID        int64    `pk:"autoincr"`
	Name      string
}
