package db

import (
	"fmt"

	conf "bitbucket.org/parkingcrew/parking/conf"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func init() {
	pgDBSesion()
	if conf.Conf.AppMod == "prod" {
		DbSession.LogMode(false)
	}
}

//DbSession - globaly
var DbSession *gorm.DB

//CocrouchDBSesion - its dbsesion
func pgDBSesion() {
	db, err := gorm.Open("postgres", conf.Conf.CocrouchDbConnectionString())
	if err != nil {
		panic(fmt.Sprintf("%v", err))
	}
	DbSession = db
}
